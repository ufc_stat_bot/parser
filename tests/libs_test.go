package tests

import (
	"reflect"
	"testing"

	"github.com/doctorImax/parser/libs"
)

func TestRevision(t *testing.T) {
	type atomStruct struct {
		Name   string
		Rank   string
		Record string
	}
	type nestedStruct struct {
		Fighter atomStruct
	}
	type testStruct struct {
		Division nestedStruct
	}

	args := testStruct{Division: nestedStruct{atomStruct{
		Name:   "Aleksandr",
		Rank:   "1",
		Record: "0-1",
	}}}
	emptyNameCase := args
	emptyRecordCase := args
	emptyNameCase.Division.Fighter.Name = ""
	emptyRecordCase.Division.Fighter.Record = ""
	tests := []struct {
		name string
		args testStruct
		want string
	}{
		{
			"#1 should path to empty name field",
			emptyNameCase,
			"[Division] ==> [Fighter] ==> [Name]",
		},
		{
			"#1 should path to empty record field",
			emptyRecordCase,
			"[Division] ==> [Fighter] ==> [Record]",
		},
	}
	for _, test := range tests {
		got, _ := libs.Revision(reflect.TypeOf(test.args), reflect.ValueOf(test.args))
		if got != test.want {
			t.Errorf("got %v but want %v\n", got, test.want)
		}
	}

}
