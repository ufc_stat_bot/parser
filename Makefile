GIT_VERSION ?= $(shell git describe --abbrev=4 --dirty --always --tags)

TMPDIR ?= $(shell dirname $$(mktemp -u))
CURRENT_DIR = $(shell pwd)

NAMESPACE = doctorImax/ufc_stat_crawler/$(PACKAGE)
COVER_FILE ?= $(TMPDIR)/$(PACKAGE)-coverage.out


.PHONY: lint
lint: ## Run golang ci linter
	echo $(CURRENT_DIR)
	docker run --rm -v $(CURRENT_DIR):/app -w /app golangci/golangci-lint golangci-lint run -v

.PHONY: check
check: static_check test ## Check project with static checks and unit tests

.PHONY: static_check
static_check: fmt imports vet line_length_linter ## Run static checks (fmt, lint, imports, vet, ...) all over the project

.PHONY: fmt
fmt: ## Run go fmt for the whole project
	test -z $$(for d in $$(go list -f {{.Dir}} ./...); do gofmt -e -l -w $$d/*.go; done)

.PHONY: imports
imports: tools ## Check and fix import section by import rules
	test -z $$(for d in $$(go list -f {{.Dir}} ./...); do goimports -e -l -local $(NAMESPACE) -w $$d/*.go; done)

.PHONY: vet
vet: ## Check the project with vet
	go vet ./...

.PHONY: line_length_linter
line_length_linter:
	lll --maxlength 120 --tabwidth 4 --skiplist
	test -z $$(lll --maxlength 120 --tabwidth 4 --skiplist)

.PHONY: tools
tools: ## Install all needed tools, e.g. for static checks
	@echo Installing tools from tools.go
	go get github.com/walle/lll/...
	@grep '_ "' tools.go | grep -o '"[^"]*"' | xargs -tI % go install %

.PHONY: test
test: ## Run unit (short) tests
	echo $(COVER_FILE)
	go test -short ./... -coverprofile=$(COVER_FILE)
	go tool cover -func=$(COVER_FILE) | grep ^total

