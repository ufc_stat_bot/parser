module github.com/doctorImax/parser

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/google/uuid v1.3.0
)

require (
	github.com/Jeffail/gabs v1.4.0 // indirect
	github.com/alexflint/go-arg v0.0.0-20160306200701-e71d6514f40a // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/walle/lll v1.0.1 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
)
