package libs

import (
	"fmt"
	"reflect"
	"strings"
)

func Revision(t reflect.Type, v reflect.Value) (string, error) {
	const separator = "==> "
	var (
		err error
		msg string
	)

	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		fieldType := t.Field(i)
		tag := fieldType.Tag.Get("required")
		if tag != "" {
			continue
		}
		if field.Kind() == reflect.Struct {
			msg += fmt.Sprintf("[%v] %s", t.Field(i).Name, separator)
			childType := t.Field(i).Type
			msgReturn, err := Revision(childType, field)
			if err == nil {
				msg = ""
			}
			msg += msgReturn

			if err != nil {
				return strings.TrimSuffix(msg, " ==> "), err
			}
			continue
		}
		if t.Field(i).Tag.Get("required") != "" {
			continue
		}

		if field.String() == "" {
			msg += fmt.Sprintf("[%v]", t.Field(i).Name)
			err = fmt.Errorf("field with %v tag is empty", fieldType.Tag)
			return msg, err
		}
	}

	return "", err
}
