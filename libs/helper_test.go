package libs

import (
	"reflect"
	"testing"
)

func TestRevision(t *testing.T) {
	type atomStruct struct {
		Name   string
		Rank   string
		Record string
	}
	type nestedStruct struct {
		Fighter atomStruct
	}
	type testStruct struct {
		Division nestedStruct
	}

	args := testStruct{Division: nestedStruct{atomStruct{
		Name:   "Aleksandr",
		Rank:   "1",
		Record: "0-1",
	}}}
	emptyNameCase := args
	emptyRecordCase := args
	emptyNameCase.Division.Fighter.Name = ""
	emptyRecordCase.Division.Fighter.Record = ""
	tests := []struct {
		name string
		args testStruct
		want string
	}{
		{
			"#1 should return path to empty name field",
			emptyNameCase,
			"[Division] ==> [Fighter] ==> [Name]",
		},
		{
			"#2 should return path to empty record field",
			emptyRecordCase,
			"[Division] ==> [Fighter] ==> [Record]",
		},
		{
			"#2 should return empty string",
			args,
			"",
		},
	}
	for _, test := range tests {
		got, _ := Revision(reflect.TypeOf(test.args), reflect.ValueOf(test.args))
		if got != test.want {
			t.Errorf("got %v but want %v\n", got, test.want)
		}
	}

}
