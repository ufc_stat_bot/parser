package main

import "testing"

func TestStartApp(t *testing.T) {
	want := "App start"
	got := StartApp()
	if want != got {
		t.Errorf("got %v but want %v\n", got, want)
	}
}
