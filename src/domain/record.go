package domain

import "strings"

type Record struct {
	Win string `json:"win"`

	Draw string `json:"draw"`

	Loss string `json:"loss"`

	NC string `json:"NC"`
}

func NewRecord(rawRecord string) *Record {
	r := &Record{}
	r.convertRawRecord(rawRecord)
	return r
}

func (r *Record) convertRawRecord(rawRecord string) {
	splitRecord := strings.Split(rawRecord, "-")
	if len(splitRecord) > 3 {
		r.Win = splitRecord[0]
		r.Loss = splitRecord[1]
		r.Draw = splitRecord[2]
		r.NC = splitRecord[3]
	} else {
		r.Win = splitRecord[0]
		r.Loss = splitRecord[1]
		r.Draw = splitRecord[2]
	}

}
func (r *Record) Validate() error {

	return nil
}
