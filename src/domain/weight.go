package domain

import (
	"fmt"
	"strconv"
)

// WeightClass весовая категория
type WeightClass struct {

	// kilo Лимит в килограммах
	kilo int

	// pound Лимит в фунтах
	pound int
}

var (
	Lightweight = WeightClass{
		kilo:  79,
		pound: 155,
	}
	Featherweight = WeightClass{
		kilo:  66,
		pound: 145,
	}
	Bantamweight = WeightClass{
		kilo:  61,
		pound: 135,
	}
	Flyweight = WeightClass{
		kilo:  57,
		pound: 125,
	}
	Welterweight = WeightClass{
		kilo:  77,
		pound: 170,
	}
	Middleweight = WeightClass{
		kilo:  84,
		pound: 185,
	}
	LightHeavyweight = WeightClass{
		kilo:  93,
		pound: 205,
	}
	Heavyweight = WeightClass{
		kilo:  120,
		pound: 265,
	}
)

var weightClassList = map[string]WeightClass{
	"Наилегчайший вес": Flyweight,
	"Легчайший вес":    Bantamweight,
	"Полулёгкий вес":   Featherweight,
	"Легкий вес":       Lightweight,
	"Полусредний вес":  Welterweight,
	"Средний вес":      Middleweight,
	"Полутяжёлый вес":  LightHeavyweight,
	"Тяжёлый вес":      Heavyweight,
}

func NewWeightClass(divisionName string) (WeightClass, error) {
	find, ok := weightClassList[divisionName]
	if !ok {
		return WeightClass{}, fmt.Errorf("weightclass not find")
	}
	return find, nil

}

func (w *WeightClass) KiloToString() string {
	return strconv.Itoa(w.kilo)
}

func (w *WeightClass) PoundToString() string {
	return strconv.Itoa(w.pound)
}
