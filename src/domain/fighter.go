package domain

type Fighters []*Fighter

// Fighter структура отображающая данные бойца
type Fighter struct {
	// Name Имя бойца на русском языке
	Name string `json:"name"`

	// NickName прозвише спорстсмена
	NickName string `json:"nickName"`

	// Link ссылка на страницу с персональными данными бойца
	Link string `json:"link"`

	// Rank место бойца в топе
	Rank string `json:"rank"`

	// Record статистика выступлений спортсмена в промоушене
	Record *Record `json:"record"`

	// Statistic персональная статистика в UFC
	Statistic *Statistic `json:"statistic"`

	// Fight Бои в которых принял участие спортсмен
	Fight []Fight `json:"fight"`
}

func NewFighter(name string) *Fighter {
	return &Fighter{
		Name: name,
	}
}

func (f *Fighter) SetLink(link string) {
	f.Link = UrlPrefix + link
}

func (f *Fighter) SetRank(rank string) {
	f.Rank = rank
}

func (f *Fighter) SetNickName(nickName string) {
	f.NickName = nickName
}

func (f *Fighter) SetChampionStatus(status string) {
	f.Rank = status
}

func (f *Fighter) SetRecord(record string) {
	r := NewRecord(record)
	f.Record = r
}

func (f *Fighter) SetStatistic(statistic []string) {
	stats := NewStatistic(statistic)
	f.Statistic = stats
}

func (f *Fighter) Validate() error {
	return nil
}
