package domain

const (
	DivisionSelector        = ".views-table"
	DivisionName            = "h4"
	FighterSelector         = "tr"
	FighterName             = "td"
	FighterChampionSelector = ".info"
)

const (
	// UrlPrefix префикс к url сайта UFC
	UrlPrefix = "https://ru.ufc.com/"
)
