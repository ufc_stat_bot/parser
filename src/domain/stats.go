package domain

import (
	"fmt"
)

// Statistic структура представляющая статистику бойца
type Statistic struct {

	// WinStrict победная серия бойца
	WinStrict string `json:"winStrict"`

	// Knockouts количество нокаутов
	Knockouts string `json:"knockouts"`

	// FirstRoundFinish финиши в первом раунде
	FirstRoundFinish string `json:"firstRoundFinish"`

	// StrikeAccuracy точность ударов
	StrikeAccuracy string `json:"strikeAccuracy"`

	// WrestlingStat статистика в борьбе
	WrestlingStat string `json:"wrestlingStat"`
}

func NewStatistic(rawRecord []string) *Statistic {
	s := &Statistic{}
	_ = s.convertRawStatistic(rawRecord)
	return s
}

func (s *Statistic) convertRawStatistic(rawStatistic []string) error {

	if len(rawStatistic) > 3 {
		return fmt.Errorf("wrong statistics data")
	}
	s.WinStrict = rawStatistic[0]
	s.Knockouts = rawStatistic[1]
	s.FirstRoundFinish = rawStatistic[2]

	return nil
}
