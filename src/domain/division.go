package domain

import (
	"fmt"
)

const (
	// DivisionsCount количество дивизионов ufc
	DivisionsCount = 14
)

// Divisions сущность представляющая список дивизионов
type Divisions []*Division

// Division сущность представляющая дивизион
type Division struct {
	// Name Название дивизиона
	Name string

	// Weight Лимит веса
	Weight WeightClass

	Fighters Fighters
	// TopListLength Количество рейтинговых бойцов (топ-15)
	TopListLength int
}

func NewDivision(name string) *Division {
	weightClass, err := NewWeightClass(name)
	if err != nil {
		weightClass = WeightClass{
			kilo:  -1,
			pound: -1,
		}
	}
	return &Division{
		Name:   name,
		Weight: weightClass,
	}
}

func (d *Division) Validate() error {
	var err error
	switch {
	case d.Name == "":
		err = DivisionNameNotFetch
	case d.Weight.pound == -1 || d.Weight.kilo == -1:
		err = DivisionWeightClassNotFetch
	case len(d.Fighters) == 0 || len(d.Fighters) < 15:
		err = DivisionNotAllFightersFetch
	case len(d.Fighters) > 16:
		err = DivisionToMuchFightersFetch
	}
	return err

}

func (d *Division) AddFighter(f *Fighter) error {
	var errorType = "AddFighter error"
	err := f.Validate()
	if err != nil {
		return err
	}
	if len(d.Fighters) > 16 {
		return fmt.Errorf("%s: %s", errorType, "division is full")
	}
	d.Fighters = append(d.Fighters, f)
	return nil
}
