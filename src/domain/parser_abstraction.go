package domain

type CrawlerStringer interface {
	Text() string
}

type Crawlerer interface {
	Find(s string) []CrawlerStringer
}

type DivisionFetcher interface {
	ParseDivisions(divisionSelector, title string) Divisions
}
