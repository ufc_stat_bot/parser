package domain

import "errors"

type CriticalDivisionError error
type CommonDivisionError error

var (
	DivisionNameNotFetch CriticalDivisionError = errors.New("division name not fetch")
)

var (
	DivisionWeightClassNotFetch CommonDivisionError = errors.New("division weight class not fetch")
	DivisionNotAllFightersFetch CommonDivisionError = errors.New("division not all fighters fetch")
	DivisionToMuchFightersFetch CommonDivisionError = errors.New("division division to much fighters fetch")
)
