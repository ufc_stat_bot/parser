package parser

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"github.com/PuerkitoBio/goquery"
)

// Test example
func TestMultipleStruct(t *testing.T) {
	type nestedStruct struct {
		Name   string  `html:".name"`
		Rating float32 `html:".rating"`
	}
	type testStruct struct {
		Games []nestedStruct `html:".game"`
	}
	data, err := ioutil.ReadFile("testData/multiple_entries.html")
	if err != nil {
		t.Fatal(err)
	}

	htmlParser := NewParser()

	var ts testStruct
	err = htmlParser.Parse(data, &ts)
	if err != nil {
		t.Fatal(err)
	}
	want := testStruct{
		Games: []nestedStruct{{
			Name:   "Monster hunter",
			Rating: 9.8,
		}, {
			Name:   "Counter Strike",
			Rating: 9.2,
		},
		},
	}
	if !reflect.DeepEqual(want, ts) {
		t.Errorf("want %v but got %v\n", want, ts)
	}
}

// Test example
func TestDoubleMultipleStruct(t *testing.T) {
	type nestedStruct struct {
		Name   string  `html:".name"`
		Rating float32 `html:".rating"`
	}
	type testStruct struct {
		Games       []nestedStruct `html:".game"`
		DoubleGames []nestedStruct `html:".double_game"`
	}
	data, err := ioutil.ReadFile("testData/double_multiple_entries.html")
	if err != nil {
		t.Fatal(err)
	}

	htmlParser := NewParser()

	var ts testStruct
	err = htmlParser.Parse(data, &ts)
	if err != nil {
		t.Fatal(err)
	}
	want := testStruct{
		Games: []nestedStruct{{
			Name:   "Monster hunter",
			Rating: 9.8,
		}, {
			Name:   "Counter Strike",
			Rating: 9.2,
		},
		},
		DoubleGames: []nestedStruct{{
			Name:   "Monster hunter",
			Rating: 10.66,
		}, {
			Name:   "Counter Strike",
			Rating: 11.2,
		},
		},
	}
	if !reflect.DeepEqual(want, ts) {
		t.Errorf("want %v but got %v\n", want, ts)
	}
}

// TestParseAttribute should fetch html attributes like href for example with text content
func TestParseAttribute(t *testing.T) {
	// special mark with needed attribute
	type attrStruct struct {
		Value int `html:"value,attr"`
	}
	type hrefAttrStruct struct {
		Link string `html:"href,attr"`
	}

	type nestedStruct struct {
		Title   string         `html:".title"`
		Link    hrefAttrStruct `html:".name a"`
		Name    string         `html:".name"`
		Country string         `html:".country"`
		Value   attrStruct     `html:".dob"`
	}

	type testedStruct struct {
		Child nestedStruct `html:"#entry"`
	}

	want := testedStruct{Child: nestedStruct{
		Title:   "Mr",
		Link:    hrefAttrStruct{Link: "https://www.digitalocean.com/"},
		Name:    "Tan Nguyen",
		Country: "Vietnam",
		Value:   attrStruct{Value: 1989},
	}}

	data, err := ioutil.ReadFile("testData/attribute.html")
	if err != nil {
		t.Error(err)
	}
	htmlParser := NewParser()

	var got testedStruct
	err = htmlParser.Parse(data, &got)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v but want %v\n", got, want)
	}
}

// TestFetchRandomDivision should fetch one division be selector path
func TestFetchRandomDivision(t *testing.T) {
	type testedStruct struct {
		Flyweight RegularDivision `html:"table:nth-child(53)"`
	}

	data, err := ioutil.ReadFile("testData/wiki_all_divisions.html")
	if err != nil {
		t.Fatal(err)
	}

	want := testedStruct{}
	parser := NewParser()
	_ = parser.Parse(data, &want)

	division := reflect.ValueOf(want).FieldByName("Flyweight")
	//check that all fields are fully
	for divisionIndex := 0; divisionIndex < division.NumField(); divisionIndex++ {
		fighter := division.Field(divisionIndex)
		fighterType := fighter.Type()

		for fighterIndex := 0; fighterIndex < fighter.NumField(); fighterIndex++ {
			field := fighter.Field(fighterIndex)
			fieldType := field.Type()
			fieldTag := fighterType.Field(fighterIndex).Tag.Get("required")
			if fieldTag != "" {
				continue // not required field
			}
			if fieldType.Kind() == reflect.Struct {
				for fieldIndex := 0; fieldIndex < field.NumField(); fieldIndex++ {
					href := field.Field(fieldIndex).String()
					if href == "" {
						t.Errorf("error on field [%v] in table [%v] with rank [%v]\n",
							fighterType.Field(fighterIndex).Name,
							"Flyweight",
							fighter.FieldByName("Rank"),
						)
					}
				}
			}
			if field.String() == "" {
				t.Errorf("error on field [%v] in table [%v] with rank [%v]\n",
					fighterType.Field(fighterIndex).Name,
					"Flyweight",
					fighter.FieldByName("Rank"),
				)

			}
		}
	}
}

// Should fetch all weight classes by css selectors
func TestAllDivisionsFieldsRequired(t *testing.T) {

	type testedStruct struct {
		P4P              P4PDivision     `html:"table:nth-child(8)"`
		WP4P             P4PDivision     `html:"table:nth-child(11)"`
		Heavyweight      RegularDivision `html:"table:nth-child(17)"`
		LightHeavyweight RegularDivision `html:"table:nth-child(23)"`
		Middleweight     RegularDivision `html:"table:nth-child(29)"`
		Welterweight     RegularDivision `html:"table:nth-child(35)"`
		Lightweight      RegularDivision `html:"table:nth-child(41)"`
		Featherweight    RegularDivision `html:"table:nth-child(47)"`
		Bantamweight     RegularDivision `html:"table:nth-child(53)"`
		Flyweight        RegularDivision `html:"table:nth-child(59)"`
		WFlyweight       RegularDivision `html:"table:nth-child(71)"`
		WStrawweight     RegularDivision `html:"table:nth-child(77)"`
		WBantamweight    RegularDivision `html:"table:nth-child(65)"`
	}

	data, err := ioutil.ReadFile("testData/wiki_all_divisions.html")
	if err != nil {
		t.Fatal(err)
	}

	want := testedStruct{}

	parser := NewParser()
	_ = parser.Parse(data, &want)

	ufcDivs := reflect.ValueOf(want)
	ufcDivsType := ufcDivs.Type()
	for ufcDivsIndex := 0; ufcDivsIndex < ufcDivs.NumField(); ufcDivsIndex++ {
		division := ufcDivs.Field(ufcDivsIndex)
		divisionType := ufcDivsType.Field(ufcDivsIndex)
		for divisionIndex := 0; divisionIndex < division.NumField(); divisionIndex++ {
			fighter := division.Field(divisionIndex)
			fighterType := fighter.Type()

			for fighterIndex := 0; fighterIndex < fighter.NumField(); fighterIndex++ {
				field := fighter.Field(fighterIndex)
				fieldType := field.Type()
				fieldTag := fighterType.Field(fighterIndex).Tag.Get("required")
				if fieldTag != "" {
					continue // not required field
				}
				if fieldType.Kind() == reflect.Struct {
					for fieldIndex := 0; fieldIndex < field.NumField(); fieldIndex++ {
						href := field.Field(fieldIndex).String()
						if href == "" {
							t.Errorf("error on field [%v] in table [%v] with rank [%v]\n",
								fighterType.Field(fighterIndex).Name,
								divisionType.Name,
								fighter.FieldByName("Rank"),
							)
						}
					}
				}
				if field.String() == "" {
					t.Errorf("error on field [%v] in table [%v] with rank [%v]\n",
						fighterType.Field(fighterIndex).Name,
						divisionType.Name,
						fighter.FieldByName("Rank"),
					)

				}
			}

		}
	}

}

func TestPersonRecord(t *testing.T) {
	type nestedStruct struct {
		R []RecordsHeader `html:"tr"`
	}

	type testedStruct struct {
		Table nestedStruct `html:"table"`
	}

	data, err := ioutil.ReadFile("testData/wiki_person.html")
	if err != nil {
		t.Fatal(err)
	}

	want := testedStruct{}

	parser := NewParser()
	_ = parser.Parse(data, &want)

	t.Logf("result %#v\n", want)
	// bio selector #bodyContent  table:nth-child(4)
}

func TestTableSelector(t *testing.T) {

	var linksSlice = []string{
		"https://en.wikipedia.org/wiki/Kamaru_Usman",
		"https://en.wikipedia.org/wiki/Alexander_Volkanovski",
		"https://en.wikipedia.org/wiki/Israel_Adesanya",
		"https://en.wikipedia.org/wiki/Charles_Oliveira",
		"https://en.wikipedia.org/wiki/Francis_Ngannou",
	}

	type nestedStruct struct {
		R RecordsHeader `html:"tr"`
	}
	type testedStruct struct {
		R []Record `html:"tr:not(:nth-child(1))"`
	}
	header := RecordsHeader{
		Result:   "Res.",
		Record:   "Record",
		Opponent: "Opponent",
		Method:   "Method",
		Event:    "Event",
		Round:    "Round",
		Date:     "Date",
		Time:     "Time",
		Location: "Location",
		Notes:    "Notes",
	}
	for _, v := range linksSlice {
		resp, err := http.Get(v)
		if err != nil {
			t.Error(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Error(err)
		}

		doc, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
		if err != nil {
			t.Fatal(err)
		}
		doc.Find("table").Each(func(i int, selection *goquery.Selection) {
			var ns nestedStruct
			var ts testedStruct
			err := recursivelyParseDoc(selection, &ns, nil)
			if err != nil {
				t.Fatal(err)
			}
			if ns.R == header {
				err = recursivelyParseDoc(selection, &ts, nil)
				if err != nil {
					t.Error(err)
				}
				if len(ts.R) == 0 {
					t.Errorf("empty parse iteration")
				}
			}
		})
		resp.Body.Close()
	}
}
