package parser

type P4PDivision struct {
	One      FighterP4P `html:"tr:nth-child(3)"`
	Two      FighterP4P `html:"tr:nth-child(4)"`
	Three    FighterP4P `html:"tr:nth-child(5)"`
	Four     FighterP4P `html:"tr:nth-child(6)"`
	Fifth    FighterP4P `html:"tr:nth-child(7)"`
	Six      FighterP4P `html:"tr:nth-child(8)"`
	Seven    FighterP4P `html:"tr:nth-child(9)"`
	Eight    FighterP4P `html:"tr:nth-child(10)"`
	Nine     FighterP4P `html:"tr:nth-child(11)"`
	Ten      FighterP4P `html:"tr:nth-child(12)"`
	Eleven   FighterP4P `html:"tr:nth-child(13)"`
	Twelve   FighterP4P `html:"tr:nth-child(14)"`
	Thirteen FighterP4P `html:"tr:nth-child(15)"`
	Fourteen FighterP4P `html:"tr:nth-child(16)"`
	Fifteen  FighterP4P `html:"tr:nth-child(17)"`
}
type RegularDivision struct {
	Champion Fighter `html:"tr:nth-child(3)"`
	One      Fighter `html:"tr:nth-child(4)"`
	Two      Fighter `html:"tr:nth-child(5)"`
	Three    Fighter `html:"tr:nth-child(6)"`
	Four     Fighter `html:"tr:nth-child(7)"`
	Fifth    Fighter `html:"tr:nth-child(8)"`
	Six      Fighter `html:"tr:nth-child(9)"`
	Seven    Fighter `html:"tr:nth-child(10)"`
	Eight    Fighter `html:"tr:nth-child(11)"`
	Nine     Fighter `html:"tr:nth-child(12)"`
	Ten      Fighter `html:"tr:nth-child(13)"`
	Eleven   Fighter `html:"tr:nth-child(14)"`
	Twelve   Fighter `html:"tr:nth-child(15)"`
	Thirteen Fighter `html:"tr:nth-child(16)"`
	Fourteen Fighter `html:"tr:nth-child(17)"`
	Fifteen  Fighter `html:"tr:nth-child(18)"`
}

type Division struct {
	Roll []FighterP4P `html:"tr:nth-child(3)"`
}

type Divisions struct {
	Table []Division `html:".wikitable"`
}
