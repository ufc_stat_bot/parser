package parser

// Record отражения всех статистических данных и результатов боев в рекордах спортсмена
type Record struct {
	Result       string `html:"td:nth-child(1)"`
	Record       string `html:"td:nth-child(2)"`
	Opponent     string `html:"td:nth-child(3)"`
	OpponentLink Link   `html:"td:nth-child(3) a"`
	Method       string `html:"td:nth-child(4)"`
	Event        string `html:"td:nth-child(5)"`
	EventLink    Link   `html:"td:nth-child(5) a"`
	Date         string `html:"td:nth-child(6)"`
	Time         string `html:"td:nth-child(7)"`
	Location     string `html:"td:nth-child(8)"`
	Notes        string `html:"td:nth-child(9)" required:"false"`
}

type Records struct {
	R []Record `html:"tr:not(:nth-child(1))"`
}

// RecordsHeader структура необходимая для поиска нужной таблицы
// методом совпадения количества заголовкой столбцов и их тайтлов
type RecordsHeader struct {
	Result   string `html:"th:nth-child(1)"`
	Record   string `html:"th:nth-child(2)"`
	Opponent string `html:"th:nth-child(3)"`
	Method   string `html:"th:nth-child(4)"`
	Event    string `html:"th:nth-child(5)"`
	Date     string `html:"th:nth-child(6)"`
	Round    string `html:"th:nth-child(7)"`
	Time     string `html:"th:nth-child(8)"`
	Location string `html:"th:nth-child(9)"`
	Notes    string `html:"th:nth-child(10)"`
}
