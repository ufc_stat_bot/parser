package parser

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

// FighterP4P структура отражающая данные бойца представленные в рейтинговых таблицах
// поле 'required' нужно для его пропуска методом валидации
type FighterP4P struct {
	Rank            string `html:"th:nth-child(1)"`
	Name            string `html:"td:nth-child(3)"`
	PersonalLink    Link   `html:"td:nth-child(3) a"`
	Record          string `html:"td:nth-child(4)"`
	WinStreak       string `html:"td:nth-child(5)"`
	WeightClass     string `html:"td:nth-child(7)"`
	WeightClassLink Link   `html:"td:nth-child(7) a"`
	Status          string `html:"td:nth-child(8)" required:"false"`
	Event           string `html:"td:nth-child(9)" required:"false"`
	Opponent        string `html:"td:nth-child(10)" required:"false"`
}

type Fighter struct {
	Rank         string `html:"th:nth-child(1)"`
	Name         string `html:"td:nth-child(3)"`
	PersonalLink Link   `html:"td:nth-child(3) a"`
	Record       string `html:"td:nth-child(4)"`
	PreviousRank string `html:"td:nth-child(5)" required:"false"`
	WinStreak    string `html:"td:nth-child(6)"`
	LastFight    string `html:"td:nth-child(7)"`
	LastEvent    string `html:"td:nth-child(8)"`
	LastOpponent string `html:"td:nth-child(9)"`
	Event        string `html:"td:nth-child(11)"`
	Opponent     string `html:"td:nth-child(12)" required:"false"`

	PersonalRecord *Records
}

var peronalRecordSample = RecordsHeader{
	Result:   "Res.",
	Record:   "Record",
	Opponent: "Opponent",
	Method:   "Method",
	Event:    "Event",
	Round:    "Round",
	Date:     "Date",
	Time:     "Time",
	Location: "Location",
	Notes:    "Notes",
}

func (f Fighter) Validate() error {
	if f.PersonalLink.Href == "" {
		return fmt.Errorf("empty lerson link %v\n", f.Name)
	}
	return nil
}

func (f Fighter) httpRequest() ([]byte, error) {
	resp, err := http.Get(f.PersonalLink.Href)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, resp.Body.Close()
}

func (f *Fighter) PersonMMARecord() error {
	body, err := f.httpRequest()
	if err != nil {
		return err
	}
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
	if err != nil {
		return err
	}
	var sample RecordsHeader
	f.PersonalRecord = &Records{}
	doc.Find("table").Each(func(i int, selection *goquery.Selection) {
		_ = recursivelyParseDoc(selection, &sample, nil)
		if sample == peronalRecordSample {
			_ = recursivelyParseDoc(selection, f.PersonalRecord, nil)
		}
	})
	return nil
}
