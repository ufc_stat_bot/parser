package parser

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func mockServer(payload []byte) *httptest.Server {
	f := func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/plain")
		_, _ = w.Write(payload)
	}
	return httptest.NewServer(http.HandlerFunc(f))
}

// Простой 'Hello World' тест с моком сервера
func TestFighter_HTTPRequest(t *testing.T) {
	payload := []byte("Hello World!")
	server := mockServer(payload)
	defer server.Close()
	link := Link{Href: server.URL}

	var f Fighter
	f.PersonalLink = link
	err := f.Validate()
	if err != nil {
		t.Error(err)
	}

	got, err := f.httpRequest()
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(got, payload) {
		t.Errorf("got %v but want %v\n", got, payload)
	}
}

func TestFighter_Volkanovski_PersonRecord(t *testing.T) {
	wantFightCount := 25
	payload, err := ioutil.ReadFile("testData/wiki_person.html")
	if err != nil {
		t.Error(err)
	}

	server := mockServer(payload)
	defer server.Close()

	link := Link{Href: server.URL}

	var f Fighter
	f.PersonalLink = link
	err = f.Validate()
	if err != nil {
		t.Error(err)
	}

	err = f.PersonMMARecord()
	if err != nil {
		t.Error(err)
	}

	if len(f.PersonalRecord.R) != wantFightCount {
		t.Errorf("got %v but wantFightCount %v \n", len(f.PersonalRecord.R), wantFightCount)
	}
}

func TestFighter_Tai_PersonRecord(t *testing.T) {
	wantFightCount := 19
	payload, err := ioutil.ReadFile("testData/wiki_person_tai.html")
	if err != nil {
		t.Error(err)
	}

	server := mockServer(payload)
	defer server.Close()

	link := Link{Href: server.URL}

	var f Fighter
	f.PersonalLink = link
	err = f.Validate()
	if err != nil {
		t.Error(err)
	}

	err = f.PersonMMARecord()
	if err != nil {
		t.Error(err)
	}

	if len(f.PersonalRecord.R) != wantFightCount {
		t.Errorf("got %v but wantFightCount %v \n", len(f.PersonalRecord.R), wantFightCount)
	}
}
